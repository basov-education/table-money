# Vuejs test

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Run tests
```
yarn test
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Иллюстрация к проекту
![Иллюстрация к проекту](https://gitlab.com/basov-education/table-money/-/raw/main/desciprion/1.png)

![Иллюстрация к проекту](https://gitlab.com/basov-education/table-money/-/raw/main/desciprion/2.png)